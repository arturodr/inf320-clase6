package inf320.pucp.pe.clase6;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


public class MainActivity extends ActionBarActivity {


    private Button button;
    private TextView tempactual;
    private TextView tempmin;
    private TextView tempmax;
    private ImageView imagen;
    private ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button);
        tempactual = (TextView) findViewById(R.id.tempactual);
        tempmin = (TextView) findViewById(R.id.tempmin);
        tempmax = (TextView) findViewById(R.id.tempmax);
        imagen = (ImageView) findViewById(R.id.imageView);
        pb = (ProgressBar) findViewById(R.id.progressBar);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pb.setVisibility(View.VISIBLE);

                //Llamar a la tarea asíncrona
                TareaClima tarea = new TareaClima();
                tarea.execute();
            }
        });
    }

    private class TareaClima extends AsyncTask<Void, Void, BeanClima> {

        @Override
        protected BeanClima doInBackground(Void... params) {

            BeanClima clima = new BeanClima();
            String datos = (new DatosOpenweathermap()).climaLima();

            try {
                JSONObject json = new JSONObject(datos);
                //TODO Verificar si se retorna el valor correcto


                JSONArray jsonArray = json.getJSONArray("list");
                JSONObject jsonClima = jsonArray.getJSONObject(0);

                clima.setTActual(jsonClima.getJSONObject("main").getString("temp"));
                clima.setTMin(jsonClima.getJSONObject("main").getString("temp_min"));
                clima.setTMax(jsonClima.getJSONObject("main").getString("temp_max"));

                clima.setIcono(jsonClima.getJSONArray("weather").getJSONObject(0).getString("icon"));

                clima.setImagen(DatosOpenweathermap.obtenerImagen(clima.getIcono()));


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return clima;
        }


        @Override
        protected void onPostExecute(BeanClima clima){
            super.onPostExecute(clima);

            tempactual.setText(clima.getTActual() + " C");
            tempmin.setText(clima.getTMin() + " C");
            tempmax.setText(clima.getTMax() + " C");

            imagen.setImageBitmap(clima.getImagen());

            pb.setVisibility(View.INVISIBLE);
        }
    }
}
