package inf320.pucp.pe.clase6;

import android.graphics.Bitmap;

/**
 * Created by arturodr on 5/5/15.
 */
public class BeanClima {
    private String TActual;
    private String TMax;
    private String TMin;
    private String icono;
    private Bitmap imagen;


    public String getTActual() {
        return TActual;
    }

    public void setTActual(String TActual) {
        this.TActual = TActual;
    }

    public String getTMax() {
        return TMax;
    }

    public void setTMax(String TMax) {
        this.TMax = TMax;
    }

    public String getTMin() {
        return TMin;
    }

    public void setTMin(String TMin) {
        this.TMin = TMin;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public Bitmap getImagen() {
        return imagen;
    }

    public void setImagen(Bitmap imagen) {
        this.imagen = imagen;
    }
}
