package inf320.pucp.pe.clase6;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by arturodr on 5/5/15.
 */
public class DatosOpenweathermap {

    private final String URL_API = "http://api.openweathermap.org/data/2.5/find?q=Lima,PE&units=metric";
    private final String URL_ICO = "http://openweathermap.org/img/w/";


    public String climaLima() {
        HttpURLConnection con;
        InputStream is;

        try {
            con = (HttpURLConnection) (new URL(URL_API)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.connect();

            // Respuesta del API
            StringBuffer sb = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String linea;

            while ( (linea = br.readLine()) != null) {
                sb.append(linea + "\r\n");
            }
            is.close();
            con.disconnect();
            return sb.toString();


        } catch (Throwable t) {
            t.printStackTrace();
        }

        return "";
    }

    public static Bitmap obtenerImagen(String codigo) throws IOException {
        String url = "http://openweathermap.org/img/w/" + codigo + ".png";
        Bitmap bmp = BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, null);

        return bmp;
    }

}
